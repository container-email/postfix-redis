ARG DVER=3.18

################################
FROM docker.io/alpine:$DVER
ARG APKVER

LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"

USER root

WORKDIR /tmp/build
RUN V=$(cat /etc/alpine-release) \
&& echo "downloading for alpine $V: postfix/$V/postfix-$V.tar.bz2" \
&& wget -q --output-document - https://gitlab.com/api/v4/projects/46194925/packages/generic/postfix/$V/postfix-$V.tar.bz2 | tar -xvj \
&& ls -lah /tmp/build/postfix \
&& apk update \
&& apk upgrade --available --no-cache \
&& cp keys/*.pub /etc/apk/keys \
&& echo "/tmp/build/postfix" >> /etc/apk/repositories \
&& apk add -u --no-cache ca-certificates icu-data-full openssl postfix postfix-redis stunnel \
&& rm -rf /tmp/build \
&& mkdir /var/spool/postfix/etc \
&& cp /etc/services /var/spool/postfix/etc/services \
&& newaliases

WORKDIR /etc/postfix
COPY --chmod=644 conf.d/* ./

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh entrypoint.sh ./
COPY --chmod=644 stunnel.conf /etc/stunnel/stunnel.conf

CMD [ "entrypoint.sh" ]

EXPOSE 25 587
VOLUME [ "/var/lib/postfix" ]

HEALTHCHECK --start-period=60s --interval=600s CMD netstat -l -t | grep -q smtp
